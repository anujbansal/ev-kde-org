---
title: "Joining KDE e.V. Trusted IT Consulting Firms"
layout: page
---

## How to Join

Joining the <a href="/consultants.html">KDE e.V. Trusted IT Consulting Firms</a>
list is easy. All you have to do is send us an email at
<a href="mailto:consulting@kde.org">consulting@kde.org</a> explaining which are
your strong points within the KDE Community.

After a quick verification that you know the KDE development community and
processes we will get in contact to finalize getting your logo and description
ready so we can add them to the list.

We will also make sure we get in touch with you for any project we get
contacted about.

### How to keep being listed

We will contact you once a year to make sure your description is still current and
that you still want to be listed.

Of course if you do have any question or want to amend your description on the list,
feel free to do via <a href="mailto:consulting@kde.org">consulting@kde.org</a>

## Finders Fee

We will not ask for any kind of compensation for you to be listed at the
<a href="consultants.html">KDE e.V. Trusted IT Consulting Firms list</a> neither
expect any kind of monetary benefit if you get a project through us.

We are happy with companies thriving :) Of course if you want to share some of
your wealth you can always become a <a href="supporting-members.html">Supporting Member</a>.
