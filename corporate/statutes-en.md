<?php
 $page_title = "Moved";
 $site_menus = 1;
 include "header.inc";
?>

<p>The page you have requested has moved.</p>

<p><a href="statutes.php">Go to new location.</a></p>

<?php
 include "footer.inc";
?>
