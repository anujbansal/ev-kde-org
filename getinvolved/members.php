<?php
 $page_title = "Become an active  KDE e.V. Member";
  $showedit=false;
 include "header.inc";
?>

<p>To become a KDE e.V. member, find one of its member to suggest you, with the 
support of two other members, to the membership, which will then <a 
href="/rules/online_voting.php">vote about your request</a>. You will be 
asked to fill in <a href="/resources/ev-questionnaire.text">this informal 
questionnaire</a>. During discussion period for your membership application 
you might also be asked by e.V. members for clarifications for certain 
questionaire items. Should that happen, please respond with as much relevant 
details as possible.</p>

<p>See the <a href="../members.php">list of members</a> to find out who already
is a member of the KDE e.V.</p>

<?php
 include "footer.inc";
?>
