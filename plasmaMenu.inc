<?php
global $document_root, $plasmaMenu;

// $plasmaMenu->iconPath = "/images/icons";

$plasmaMenu->addMenu("Information", false, "green.icon.png", "#acff08");
  $plasmaMenu->addMenuEntry("About Us", "/index.php");
  $plasmaMenu->addMenuEntry("What is KDE e.V.?", "/whatiskdeev.php");
  $plasmaMenu->addMenuEntry("News", "/news.php");
  $plasmaMenu->addMenuEntry("Announcements", "/announcements");
  $plasmaMenu->addMenuEntry("General Assembly", "/generalassembly");
  $plasmaMenu->addMenuEntry("Reports", "/reports");
  $plasmaMenu->addMenuEntry("Get Involved", false);
  $plasmaMenu->addMenuEntry("Trusted IT Consulting Firms", "/consultants.php");
    $plasmaMenu->addSubMenuEntry("Membership", "/getinvolved");
    $plasmaMenu->addSubMenuEntry("Active Member","/getinvolved/members.php");
    $plasmaMenu->addSubMenuEntry("Supporting Member","/getinvolved/supporting-members.php");

$plasmaMenu->addMenu("Activities", false, "orange.icon.png", "#ffae00");
  $plasmaMenu->addMenuEntry("Akademy", "/akademy");
  $plasmaMenu->addMenuEntry("Developer Meetings","/activities/devmeetings");
  $plasmaMenu->addMenuEntry("FLA (Licensing)","/rules/fla.php");
  $plasmaMenu->addMenuEntry("Community Partnership","/activities/partnershipprogram.php");
  $plasmaMenu->addMenuEntry("More...", "/activities");

$plasmaMenu->addMenu("Documents", false, "red.icon.png", "#ff96af");
  $plasmaMenu->addMenuEntry("Articles of Association","/corporate/statutes.php");
  $plasmaMenu->addMenuEntry("Rules and Policies","/rules");
    $plasmaMenu->addSubMenuEntry("Supporting Members","/rules/supporting_members.php");
    $plasmaMenu->addSubMenuEntry("Online Voting","/rules/online_voting.php");
    $plasmaMenu->addSubMenuEntry("FLA","/rules/fla.php");
    $plasmaMenu->addSubMenuEntry("Reimbursement Policy","/rules/reimbursement_policy.php");
    $plasmaMenu->addSubMenuEntry("KDE Sprint Policy","/rules/sprint_policy.php");
    $plasmaMenu->addSubMenuEntry("Thank You Policy","/rules/thankyou_policy.php");
	$plasmaMenu->addMenuEntry("Forms","/resources");


$plasmaMenu->addMenu("Organization", false, "gray.icon.png", "#aaa");
  $plasmaMenu->addMenuEntry("Board of Directors","/corporate/board.php");
  $plasmaMenu->addMenuEntry("Members","/members.php");
  $plasmaMenu->addMenuEntry("Supporting Members","/supporting-members.php");
  $plasmaMenu->addMenuEntry("Working Groups","/workinggroups");
    $plasmaMenu->addSubMenuEntry("Advisory Board","/workinggroups/abwg.php");
    $plasmaMenu->addSubMenuEntry("Community","/workinggroups/cwg.php");
    $plasmaMenu->addSubMenuEntry("Financial","/workinggroups/fwg.php");
    $plasmaMenu->addSubMenuEntry("Fundraising","/workinggroups/fundraisingwg.php");
    $plasmaMenu->addSubMenuEntry("System Administration","/workinggroups/sysadmin.php");
  $plasmaMenu->addMenuEntry("Affiliates","/affiliates.php");
  $plasmaMenu->addMenuEntry("Advisory Board","/advisoryboard.php");
  $plasmaMenu->addMenuEntry("Community Partners","/community-partners.php");


$plasmaMenu->addMenu("Contact", false, "purple.icon.png", "#e285ff");
  $plasmaMenu->addMenuEntry("Contact Us", "/contact.php");
  $plasmaMenu->addMenuEntry("Donations","/donations.php");
?>
