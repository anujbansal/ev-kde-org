<?php
$page_title = "2004 Meeting Notes";
include "header.inc";
?>

<p><em>These are very rough notes from the meeting, taken by Jonathan Riddell</em></p>

<p>Matthias Kalle Dalheimer will be chair, passed by a large show of hands.  Eva Brucherseifer will be secretary, no votes against.  Mirko Boehm declares the meeting opened.</p>

<p>Are we quorum?  We need 41 people to be 50%, we seem to be so.  Voting in proxy rules didn't work in time so organising that was all in vain.</p>

<p>Mirko posted a report about accounts last year so there's not much news just now.  Last year we opened with 8000ish euro.  Spent 2811 euro on travel, 1500euro on trademark registrations in EU and US, €2000 on travel to go to meetings and a machine, €2300 euro on a press account.  </p>

<p>Dirk Mueller said there is not much progress with the professional press release account but KDE advertised the akademy event and press releases since May have been about that.  Nothing else has been accepted by the press syndicators, they need something to advertise.  Ian Geiser asked if there should be a group of people making press releases, Dirk Mueller said he posted on KDE promo.  Feedback from the press has mostly been good although so far it has mostly been only in German.  Would it make sense to target English first, not yet small steps only.  We don't need to do writing just need content, e.g. if there is a major new feature in an application, anything ready for submitting send to them or Dirk.  Question if developers know about this possibility, not really sure.  We should have a meeting about this through the week and Mirko said we should have a sub group, Fabrice Mous said there is a press office during this meeting.</p>

<p>9500Euro spend on different purposes.  14500euro income, mostly donations.  Final balance until June 30th 2004 is €11890, don't know how much money we will have after this event but expect about €8000.  Before the next meeting he expects more than €10000 which is enough for the next year.  No questions.</p>

<p>Lots of bureaucracy with trademarks including the EU rejecting it because there were too many Google hits for KDE, Kalle tried to convince them that they all point to us.  TM application is ready for publication so that will happen sometime depending on the speed of the TM office then other people can contest it, if nobody does then the trademark is ours.  In the US the application is a priority application based on the EU application so the US is waiting on the EU, similar questions were asked about Google hits for KDE.  The thought was that because KDE has so many hits it is a generic term, Kalle proved that all the Google hits are not a general term but speaking about KDE's stuff by showing that links are going back to our website.  The reason for getting the trademark is so that nobody else can get it but then we need to work out some means for allowing people to use the TM KDE.  There shouldn't be an official licence, occasionally e.V. has been asked for a licence by magazines etc but they are just rejected.  Do we have to add "TM" to all our logos?  No that's not necessary, it's just a way to make clear that it's a trademark.  But if we don't then it may be seen as not enforcing our trademark.  (It's the (R) we want, TM doesn't have to be registered.)</p>

<p>Eva talks about preparation of this conference.  Preparation started last year, some companies were approached and we are working with Linux New Media, this was fortunate as they are helping us a lot and not just wanting a profit.  Local group of Chaos Computer Club is running the network.  Local LUGs are helping.  If we want to do such an event again we will need someone who is working part time on it.  Budget is about €70000 euro turnover, most of that is from the sponsorship of companies.  We are glad that Linux New Media are doing the accounting so it doesn't go on e.V.'s books, simpler.  Applause for Eva.  Standing ovation for Eva.</p>

<p>Delay of by-laws (changed last year to allow proxy voting) was due to personal problems from Ralf Nolden so Eva had to do it and she was too busy preparing the event.  Ralf will turn up later today.  Eva has tried to phone people who were away but it will happen soon.</p>

<p>Lars and Binner get up for the report of the auditors.  They looked through the accounts, had a few questions but these were all cleared up easily.  </p>

<p>Requirements for getting stuff reimbursed, we are under close scrutiny from financial authorities, originals are needed for any claims.  We are currently under a money laundering probe because registration fees were higher than expected.  We need to be careful in accounting because we are trying to get tax exempt status in Germany, this requires more than just existing it needs a full accounting report each year to the authorities, this is only possibly with original receipts.  Other reason is that we are reaching a cash flow which is over that of a private operation, e.g. paypal account got over 5000euro in one month.  Do we need to do a special delivery when posting tickets, probably not but take a copy and send by normal post.</p>

<p>The board suggests that Alexandra Boehm is made a member of e.v. so
she can help with accounts at no cost.  (Tax administrators can only
work for other entities if they are contracted or are a member of the
organisation.)  All agree.</p>

<p>Question from KDE-NL, can we run our own organisation selling stuff from KDE e.v. and keeping the profits.  That's possible but they could also allow a budget within KDE e.v. so a second organisation does not have to be registered.  This topic should be delayed until we discuss the issue generally later in the meeting.</p>

<p>That concludes the board's statement.  Do we approve the work of the board since last year? This was unanimously approved.</p>

<p>Report from reps to KDE League.  Daniel M stands up because Scott isn't here.  Not much of an update since last meeting.  Waiting for KDE-US association being created then can work out what to do with remaining money and wind up KDE League.  Geiser says that KDE-US is stuck with a lack of focus of what it would do other than just "replace KDE League", costs about $128 to complete the paperwork but needs incentive and working out what to do and how to do it. Eric Lafoon says it would cost several thousand dollars to set up US tax exempt stuff.  Eric and Ian discuss lots of legal bits about KDE-US.  Do we approve the proposal to go and investigate US tax/charity stuff?  Approved unanimously.</p>

<p>Lots of discussion about local KDE organisations, no conclusion but we decided to talk about internationalising KDE e.V. in a group later.</p>

<p>Report for KDE Free Qt Foundation by Cornelius, they updated the text and made it more precise so we are on the safe side for a Free Qt, this took about half a year of negotiations.  The foundation has not made Qt safe for proprietary developers or the proprietary version of Qt, this is something that could be discussed.  Is this a concern, it would be BSD so it can be proprietary.  Problem is if Trolltech develops the Free version but not the proprietary version, this could happen if a company takes over Trolltech and stops selling it as a product but uses it internally and releases a Free version.  This is a possible anti-KDE argument.  Maybe this is more of a Trolltech problem.  Is this possible at all since who would develop the proprietary Qt if Trolltech stopped.  Matthias said that the new version was due to a proprietary developer asking for it but suspects it doesn't make much difference in reality, the agreement in general doesn't mean much anyway now that Qt is GPL so can be freely developed anyway, it does help with PR however.</p>

<p>The board has another year left, if anyone wants to do it next year they should let them know.  Current post holders all confirmed that they will continue doing their jobs for another year.</p>

<p>Lunch.  Notice that IBM are paying for meals, ("free food?  yes!")</p>

<p>National Institute of IT in Brazil and on some Free Software committee congratulates KDE in an official letter and says well done on KDE 3.3.  They hope Brazil will host a future KDE conference.</p>

<p>A discussion about companies being sponsoring members of KDE e.V.  Queries about how the sponsoring members would be able to interact with KDE e.V., should they be able to come to the meetings etc.  Proposal from Martin Konold that the board makes preparations for an advisory board, this was accepted but not unanimously, it was then delegated back to Martin.</p>

<p>Now individual supporting members who pay €100 and get the right to come to the next e.V. meeting (which will generally cost a lot more than €100 euro to come to).  The hope is this would encourage annual donors.</p>

<p>Discussion about merchandise and issues of giving money to people to sort out merchandise for local events.  This discussion was moved to the mailing list.</p>

<p>Discussion on whether Ralf Nolden should continue on the KDE board, personal problems have stopped him doing work for KDE e.V. for the last 6 months.  He resigned after Harry Porten said he could take over.  Standing ovation for Ralf, standing ovation for Harry.</p>

<p>Who should run next conference?</p>

<p>Presentation on Malaga, Southern Spain from Antonio Larossa, the director of this company said he can use the local university with all the rooms and bandwidth with 250 places for sleeping.  There will be interest from the local government and from IBM.  Are there smaller companies who might be interested, yes probably.  Will it be too hot, maybe.  Cost €280 from Spain to Germany.</p>

<p>Helio presented Brazil as a conference location which is supported by Brazilian government.  Cost will be 3 times lower than here for staying but travel costs higher for most.  $900 return from Germany and the rest of Europe except Norway.  Point made that Debian conference was very successful could we contact them for tips?  Yes.  September or October recommended, hot enough for beaches but cheaper tickets and not too hot.  Are there local companies who would be interested, yes there are including IBM.  Can we get pictures of the local women?  Laughter.  Do we need visas, not if from Europe unless you travel via the US.</p>

<p>Proposal that we try to get a position by the end of September.</p>

<p>Should KDE e.V. have an official position on software patents?</p>

<p>Aaron Seigo says it is critical that we do.  Helio says Brazil is free of these laws although there is a danger that discussions may be started, need to take stand that Brazil does not follow US or EU.  Martin Konold says we don't only need a political stance but also a stand on how to work with patents in the KDE project.  Ian says in the US the only way to win software patents is not to play (or to build up a large portfolio but that costs too much).  Eva says if we are infringing patents don't post them to public mailing lists, the archives will be used against us.  Martin encourages you to really talk to your politicians.  KDE e.V. decided to have an official position on software patents (against them) which will be formalised.</p>

<?php
include "footer.inc";
?>
