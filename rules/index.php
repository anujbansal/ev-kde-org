<?php
 $page_title = "Rules and Policies";
 include "header.inc";
?>

<h2>Rules of Procedure</h2>

<p>
A couple of rules of procedure extend the articles of association. They are
decided by the memership by a vote.
</p>

<ul>
  <li><a href="supporting_members.php">Supporting Members</a></li>
  <li><a href="online_voting.php">Online Voting</a></li>
  <li><a href="rules_of_procedures_board.php">Rules of Procedures of KDE e.V.
  Board</a></li>
</ul>

<h2>Other Rules and Policies</h2>

<p>Additional rules and policies handle specific areas where the KDE e.V. is
active. They are decided by the membership or the board.</p>

<ul>
  <li><a href="fla.php">Fiduciary Licensing Agreement (FLA)</a></li>
  <li><a href="reimbursement_policy.php">Travel Cost Reimbursement
  Policy</a></li>
  <li><a href="sprint_policy.php">KDE Sprint Policy</a></li>
  <li><a href="thankyou_policy.php">Thank You Policy</a></li>
</ul>

<h2>Internal Policies</h2>

<p>
There are policies which govern internal workings of KDE e.V. or the board.
</p>

<ul>
  <li><a href="ConflictofInterestPolicy.pdf">Conflict of Interest Policy</a></li>
</ul>

<?php
include "footer.inc";
?>
