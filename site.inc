<?php

// promote which site we are!
$site = "kde-ev";

// promote title to use
$site_title = i18n_var("KDE e.V.");

$site_search = false;

// track using piwik
$piwikSiteID = 7;
$piwikEnabled = true;

$site_showkdeevdonatebutton = true;
$templatepath = "chihuahua/";

$showedit=false;

$name = "The KDE e.V. Board";
$mail = "kde-ev-board@kde.org";

$menuright = array ('sitemap.php'=>'Sitemap', 'contact.php'=>'Contact Us');

$site_logo = "images/kde-ev_22.png";

?>
