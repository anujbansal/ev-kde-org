<?php
 $page_title = "Sitemap";
 include "header.inc";
?>

<h2>Information</h2>

<ul>
  <li><a href="./">Start Page</a></li>
  <li><a href="whatiskdeev.php">What is KDE e.V.?</a></li>
  <li><a href="news.php">News</a></li>
  <li><a href="reports">Reports</a></li>
  <li><a href="activities">Activities</a></li>
  <li><a href="getinvolved">Get Involved</a></li>
</ul>

<h2>Documents</h2>

<ul>
  <li><a href="corporate/statutes.php">Articles of Association</a></li>
  <li><a href="rules">Rules and Policies</a></li>
  <li><a href="resources">Forms</a></li>
</ul>

<h2>Organization</h2>

<ul>
  <li><a href="corporate/board.php">Board of Directors</a></li>
  <li><a href="supporting-members.php">Supporting Members</a></li>
  <li><a href="members.php">Members</a></li>
  <li><a href="workinggroups">Working Groups</a></li>
  <li><a href="affiliates.php">Affiliates</a></li>
  <li><a href="advisoryboard.php">Advisory Board</a></li>
  <li><a href="community-partners.php">Community Partners</a></li>
</ul>

<h2>Contact</h2>

<ul>
  <li><a href="donations.php">Donations</a></li>
  <li><a href="contact.php">Contact</a></li>
</ul>

<?php
 include "footer.inc";
?>
