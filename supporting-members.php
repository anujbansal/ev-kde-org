<?php
 $page_title = "Supporting Members";
 include "header.inc";
?>

<p>If your company or organization would like to become a supporting member please
have a look at the <a href="getinvolved/supporting-members.php">information how
to become a supporting member of the KDE e.V.</a></p>

<p>Individuals wishing to support KDE financially should consider to <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&id=9">Join the Game</a></p>

<p>Currently the supporting members of the KDE e.V. are:</p>

<h2>Patrons of KDE
<img style="width:91px;height:24px;float:right;" src="images/patron_small.png" class="clear" />
</h2>

<div class="table-wrapper">
<table class="patrons">
<tr>
<td>
<img src="http://ev.kde.org/images/supportingmembers/bluesystems.png" />
</td>
<td>
<img src="http://ev.kde.org/images/supportingmembers/google.jpg" />
</td>
</tr>
<tr>
<td>
<img src="http://ev.kde.org/images/supportingmembers/suse.png" />
</td>
<td>
<img src="http://ev.kde.org/images/supportingmembers/QtCompany.png" />
</td>
</tr>
<tr>
<td>
<img src="http://ev.kde.org/images/supportingmembers/canonical.png"/> <!--http://design.ubuntu.com/wp-content/uploads/logo-canonical_no%E2%84%A2-aubergine-hex.svg-->
</td>
<td>
<img src="http://ev.kde.org/images/supportingmembers/pia.png"/>
</td>
</tr>
<tr>
<td>
<img src="http://ev.kde.org/images/supportingmembers/enioka.png"/>
</td>
<td>
</td>
</tr>
</table>
</div>

<h2>Supporters of KDE
<img style="width:91px;height:24px;float:right;" src="images/supporter_small.png" class="clear" />
</h2>

<div class="table-wrapper">
<table class="supporters">
<tr>
<td>
<img src="/images/supportingmembers/kdab.png" />
</td>
<td>
<img src="/images/supportingmembers/basyskom.png" />
</td>
</tr>
</table>
</div>

<?php
 include "footer.inc";
?>
