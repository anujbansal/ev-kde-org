<?php
 $page_title = "KDE e.V. Consulting Working Group";
 include "header.inc";
?>

<h3 id="goals">Goals</h3>
<p>The KDE e.V. Consulting Working Group has the following tasks:</p>
<ul>
<li>Maintain a list of organisations available to provide services on KDE software and stack</li>
<li>Keep an open channel for organisations that need to hire help with KDE products.</li>
</ul>
<p>For fair and practical use of this group, participants are required to have diverse employers and backgrounds.</p>

<h3 id="members">Members</h3>
<p>The current members of the KDE e.V. Consulting Working Group are:</p>
<ul>
<li>Albert Astals Cid</li>
<li>Aleix Pol Gonzalez</li>
<li>Lydia Pintscher</li>
<li>Thomas Pfeiffer</li>
</ul>

<h3 id="contact">Contact</h3>
<p>You can contact the Consulting Working Group under <a href="mailto:consulting@kde.org">consulting@kde.org</a></p>

<?php
 include "footer.inc";
?>
